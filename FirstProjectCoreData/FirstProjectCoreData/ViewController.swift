//
//  ViewController.swift
//  FirstProjectCoreData
//
//  Created by BRYAN OCAÑA on 23/1/18.
//  Copyright © 2018 BRYAN OCAÑA. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    
    @IBOutlet weak var nameUITextField: UITextField!
    @IBOutlet weak var phoneUiTextField: UITextField!
    @IBOutlet weak var addresUITextField: UITextField!
    
    let manageObjectcontext = (UIApplication.shared.delegate as!
        AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    var singlePerson:Person?
    func savePerson() {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectcontext)
        
        let person = Person(entity: entityDescription!, insertInto: manageObjectcontext)
        
        person.addres = addresUITextField.text ?? ""
        person.name = nameUITextField.text ?? ""
        person.phone = nameUITextField.text ?? ""
        
        do {
            
            try manageObjectcontext.save()
            showMessage()
            clearFields()

        } catch {
            print("Error")
        }
    }
    
    func showMessage(){
        // Declaramos la alerta, que con iOS 8 se hace a través
        // de la clase UIAlertController tanto para UIAlertView
        // como para UIActionSheet
        var alert = "El usuario ha introducido el mensaje: \(nameUITextField.text)   \(addresUITextField.text) \(phoneUiTextField.text)"
        print(alert)
    }
    
    func findAll() {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectcontext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            
            var results = try(manageObjectcontext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            for result in results {
                
                let person = result as! Person
                
                print("Name:  \(person.name ?? "") ", terminator: "")
                print("Address:  \(person.addres ?? "") ", terminator: "")
                print("Phone:  \(person.phone ?? "") ", terminator: "")
                print()
                print()
            }
        } catch {
            
            print("Error finding all")
        }
    }

    func clearFields (){
        
        addresUITextField.text = ""
        nameUITextField.text = ""
        phoneUiTextField.text = ""
    }
    
    @IBAction func saveButton(_ sender: Any) {
        savePerson()
    }
    @IBAction func findButton(_ sender: Any) {
        
        if nameUITextField.text == ""{
            findAll()
            return
        }
        
        findOne()
        
    }
    
    func findOne (){
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectcontext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        request.entity = entityDescription
        
        let predicate = NSPredicate(format: "name = %@", nameUITextField.text!)
        
        request.predicate = predicate
        
        do {
            
            let results = try(manageObjectcontext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            if results.count > 0 {
                
                let match = results[0] as! Person
                singlePerson = match
                
                performSegue(withIdentifier: "finOneSegue", sender: self)
                //addresUITextField.text = match.addres
                //nameUITextField.text = match.name
                //phoneUiTextField.text = match.phone
                
                
            } else {
                addresUITextField.text = "Not found"
                nameUITextField.text = "Not found"
                phoneUiTextField.text = "Not found"
            }
        } catch {
            print("Error finding one")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "finOneSegue"){
            let destination = segue.destination as! findoneViewController
            destination.person = singlePerson
        }else if (segue.identifier == "findAllSegue"){
            
        }
    }
}


