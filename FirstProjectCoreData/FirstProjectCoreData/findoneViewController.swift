//
//  findoneViewController.swift
//  FirstProjectCoreData
//
//  Created by BRYAN OCAÑA on 31/1/18.
//  Copyright © 2018 BRYAN OCAÑA. All rights reserved.
//

import UIKit

class findoneViewController: UIViewController {

    @IBOutlet weak var telefonoLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBAction func deleteButton(_ sender: Any) {
        let manageObjectcontext = (UIApplication.shared.delegate as!
            AppDelegate).persistentContainer.viewContext
        
        manageObjectcontext.delete(person!)
        do {
            try manageObjectcontext.save()
            
        }catch {
            print("Error")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        telefonoLabel.text = person?.phone
        addressLabel.text = person?.addres
        // Do any additional setup after loading the view.
    }

    var person:Person?

    
}
